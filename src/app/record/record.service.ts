import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Record } from './record';

@Injectable({
  providedIn: 'root'
})
export class RecordService {

  private url: string;

  constructor(
    private http: HttpClient
  ) {
    this.url = '/api/record';
  }

  public showByItemId(itemId: number): Observable<Record[]> {
    return this.http.get<Record[]>(this.url + '/item/' + itemId)
  }

  public add(record: Record): Observable<Record> {
    return this.http.post<Record>(this.url, record);
  }

  public update(record: Record): Observable<Record> {
    return this.http.put<Record>(this.url + '/' + record.id, record);
  }

  public delete(id: number): Observable<void> {
    return this.http.delete<void>(this.url + '/' + id);
  }
}
