import { Component, Input, OnInit } from '@angular/core';
import { catDesc } from 'src/app/item/category';
import { Item } from 'src/app/item/item';
import { Record } from '../record';
import { RecordService } from '../record.service';

@Component({
  selector: 'app-record-list[item]',
  templateUrl: './record-list.component.html',
  styleUrls: ['./record-list.component.scss']
})
export class RecordListComponent implements OnInit {

  @Input()
  item?: Item;

  itemDesc: any;

  constructor(
    private recordService: RecordService
  ) { }

  ngOnInit(): void {
    this.itemDesc = catDesc.various
    if (this.item?.type) {
      this.itemDesc = (<any>catDesc)[this.item?.type]
    }
  }

  updateRecord(updatedData: Record) {
    let record = this.item?.records.find(rec => rec.id == updatedData.id);
    if (record) {
      if (this.item?.quantity && record?.quantity) {
        this.item.quantity += updatedData.quantity - record?.quantity;
      }
      for (const key in updatedData) {
        (<any>record)[key] = (<any>updatedData)[key];
      }
    }
  }

  deleteRecord(recordToRemove: Record) {
    if (recordToRemove.id) {
      this.recordService.delete(recordToRemove.id).subscribe(
        () => {
          if (this.item) {
            const idx = this.item.records.findIndex(record => record.id == recordToRemove.id)
            if (idx >= 0) {
              this.item.quantity -= recordToRemove.quantity,
                this.item.records.splice(idx, 1);
            }
          }
        }
      );
    }
  }
}
