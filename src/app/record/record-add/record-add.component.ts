import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { catDesc } from 'src/app/item/category';
import { Item } from 'src/app/item/item';
import { Record } from '../record';
import { RecordService } from '../record.service';

@Component({
  selector: 'app-record-add',
  templateUrl: './record-add.component.html',
  styleUrls: ['./record-add.component.scss']
})
export class RecordAddComponent implements OnInit {

  @Input()
  item?: Item;

  itemDesc: any;

  recordForm?: FormGroup;

  constructor(
    private recordService: RecordService,
    public activeModal: NgbActiveModal
  ) { }

  ngOnInit(): void {
    if (this.item?.type) {
      this.itemDesc = (<any>catDesc)[this.item.type] ? (<any>catDesc)[this.item.type] : catDesc.various;
    } else {
      this.itemDesc = catDesc.various;
    }
  }

  formChange(form: FormGroup) {
    this.recordForm = form;
  }

  sendForm() {
    if (this.recordForm) {
      let newRecord: Record = this.recordForm.value;
      newRecord.item = Object.assign({ id: this.item?.id });
      newRecord.type = this.item?.type;
      this.recordService.add(newRecord).subscribe({
        next: data => {
          if (this.item) {
            if (!this.item.records) {
              this.item.records = []
            }
            this.item.records.push(data)
            this.item.quantity += data.quantity;
            this.activeModal.close();
          }
        }
      })
    }
  }
}
