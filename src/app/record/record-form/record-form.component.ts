import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { noDecimalValidator, pastOrPresent } from 'src/app/validators';

@Component({
  selector: 'app-record-form',
  templateUrl: './record-form.component.html',
  styleUrls: ['./record-form.component.scss']
})
export class RecordFormComponent implements OnInit {

  form = this.fb.group({});

  @Input()
  record?: any;

  @Input()
  itemDesc: any;

  @Output()
  formEmit: EventEmitter<FormGroup> = new EventEmitter();

  constructor(
    private fb: FormBuilder
  ) { }

  ngOnInit(): void {
    this.form.valueChanges.subscribe(() => this.formEmit.emit(this.form));

    if (this.itemDesc) {
      for (const data of this.itemDesc.toDisplay) {
        this.form.addControl(
          data.property,
          new FormControl(
            this.record ? this.record[data.property] : '',
            this.setValidators(data)
          )
        );
      }
      this.form.addControl('comment', new FormControl(this.record ? this.record.comment : ''));
    }
  }

  get comment() {
    return this.form?.get('comment');
  }

  setValidators(data: any) {
    let validators = [];
    if (data['required']) {
      validators.push(Validators.required);
    }
    if (data['min'] != undefined) {
      validators.push(Validators.min(data['min']));
    }
    if (data['max'] != undefined) {
      validators.push(Validators.max(data['max']));
    }
    if (data['noDecimal']) {
      validators.push(noDecimalValidator());
    }
    if (data['date'] == 'pastOrPresent') {
      validators.push(pastOrPresent());
    }
    return validators;
  }
}
