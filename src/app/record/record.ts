import { Item } from "../item/item";

export interface Record {
  id?: number;
  date: string;
  quantity: number;
  conditioning: number;
  useByDate?: string;
  cost: number;
  comment?: string;
  type?: string
  item?: Item
}

export interface Fermentable extends Record {
  id?: number;
  ebc: number;
}

export interface Hop extends Record {
  id?: number;
  alpha: number;
  shape: string;
  harvest: number;
  origin: string;
}

export interface Yeast extends Record {
  id?: number;
  attenuation: number;
}