import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Record } from '../record';
import { RecordService } from '../record.service';

@Component({
  selector: 'app-record-edit',
  templateUrl: './record-edit.component.html',
  styleUrls: ['./record-edit.component.scss']
})
export class RecordEditComponent implements OnInit {

  @Input()
  record?: Record;

  @Input()
  itemDesc: any;

  @Output()
  updatedRecord: EventEmitter<Record> = new EventEmitter();

  recordForm?: FormGroup;

  constructor(
    private recordService: RecordService,
    public activeModal: NgbActiveModal
  ) { }

  ngOnInit(): void {
  }

  formChange(form: FormGroup) {
    this.recordForm = form;
  }

  sendForm() {
    if (this.recordForm && this.record) {
      Object.assign(this.record, this.recordForm.value);
      this.recordService.update(this.record).subscribe({
        next: data => {
          this.updatedRecord.emit(data);
          this.activeModal.close();
        }
      })
    }
  }
}
