import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AuthService } from 'src/app/auth/auth.service';
import { ConfirmationComponent } from 'src/app/modal/confirmation/confirmation.component';
import { Record } from '../record';
import { RecordEditComponent } from '../record-edit/record-edit.component';

@Component({
  selector: 'app-record',
  templateUrl: './record.component.html',
  styleUrls: ['./record.component.scss']
})
export class RecordComponent implements OnInit {

  @Input()
  record?: any;

  @Input()
  itemDesc?: any;

  @Output()
  updatedRecord: EventEmitter<Record> = new EventEmitter();

  @Output()
  removeRecord: EventEmitter<void> = new EventEmitter();

  authState = this.auth.state;

  constructor(
    private auth:AuthService,
    private modalService: NgbModal
  ) { }

  ngOnInit(): void {
  }

  edit() {
    const modalRef = this.modalService.open(RecordEditComponent, { centered: true, backdrop: 'static' });
    modalRef.componentInstance.record = this.record;
    modalRef.componentInstance.itemDesc = this.itemDesc;
    modalRef.componentInstance.updatedRecord.subscribe((updated: Record) => this.updatedRecord.emit(updated))
  }

  delete() {
    const modalRef = this.modalService.open(ConfirmationComponent, { centered: true, backdrop: 'static' });
    modalRef.componentInstance.title = 'Suppression ligne stock';
    modalRef.componentInstance.choice.subscribe((choice: boolean) => {
      if (choice) {
        this.removeRecord.emit()
      }
    })
  }
}
