import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  authState = this.auth.state;

  showChangePwd = false;

  constructor(
    private auth: AuthService
  ) { }

  ngOnInit(): void {
  }

  logout() {
    this.auth.logout().subscribe();
  }

  dropdownChange(open: boolean) {
    if (!open) {
      this.showChangePwd=false;
    }
  }
}
