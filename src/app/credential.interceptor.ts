import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable, tap } from 'rxjs';
import { Router } from '@angular/router';
import { AuthService } from './auth/auth.service';
import { environment } from 'src/environments/environment';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AlertComponent } from './modal/alert/alert.component';

@Injectable()
export class CredentialInterceptor implements HttpInterceptor {

  constructor(
    private auth: AuthService,
    private modalService: NgbModal
  ) { }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    request = request.clone({
      setHeaders: {
        'X-Requested-With': 'XMLHttpRequest'
      },
      withCredentials: true,
      url: environment.serverUrl + request.url
    });

    return next.handle(request).pipe(
      tap({
        error: (err: any) => {
          if (err instanceof HttpErrorResponse) {
            if (err.status != 401) {
              return;
            }
            if (!err.url?.endsWith('account')) {
              this.auth.logout().subscribe();
              const alertModalRef = this.modalService.open(AlertComponent, { centered: true, backdrop: 'static' });
              alertModalRef.componentInstance.text = 'La session a expiré. Veuillez-vous reconnecter.';
            }
          }
        }
      }));
  }
}
