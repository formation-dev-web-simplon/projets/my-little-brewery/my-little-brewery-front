import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Category } from '../item/category';
import { Item } from '../item/item';
import { ItemAddComponent } from '../item/item-add/item-add.component';
import { ItemService } from '../item/item.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  categories?: Category[];

  constructor(
    private itemService: ItemService,
    private modalService: NgbModal
  ) { }

  ngOnInit(): void {
    this.itemService.showAll().subscribe(data => this.categories = data);

  }

  addItem(type: string) {
    const modalRef = this.modalService.open(ItemAddComponent, { centered: true, backdrop: 'static' });
    modalRef.componentInstance.type = type;
    modalRef.componentInstance.newItem.subscribe((newItem: Item) => this.itemService.addItemToCategory(newItem, this.categories!))
  }
}
