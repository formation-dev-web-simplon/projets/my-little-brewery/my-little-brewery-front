import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NgbDropdown } from '@ng-bootstrap/ng-bootstrap';
import { passwordMatchValidator } from 'src/app/validators';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-change-pwd',
  templateUrl: './change-pwd.component.html',
  styleUrls: ['./change-pwd.component.scss']
})
export class ChangePwdComponent implements OnInit {

  form = this.fb.group({
    oldPassword: ['', Validators.required],
    newPassword: ['', Validators.required],
    repeatPassword: ['']
  }, { validators: passwordMatchValidator });

  hasError = false;
  constructor(
    private auth: AuthService,
    private fb: FormBuilder,
    private dropdown: NgbDropdown
  ) { }

  ngOnInit(): void {
    this.dropdown.openChange.subscribe(
      () => {
        if (!this.dropdown.isOpen()) {
          this.form.reset();
        }
      }
    )
  }

  get oldPassword() {
    return this.form.get('oldPassword');
  }
  get newPassword() {
    return this.form.get('newPassword');
  }
  get repeatPassword() {
    return this.form.get('repeatPassword');
  }

  close() {
    this.dropdown.close();
  }

  submitForm() {
    if (this.oldPassword?.value && this.newPassword?.value) {
      this.hasError = false;
      this.auth.changePassword(this.oldPassword.value, this.newPassword.value).subscribe({
        next: () => this.dropdown.close(),
        error: () => this.hasError = true
      });

    }
  }
}
