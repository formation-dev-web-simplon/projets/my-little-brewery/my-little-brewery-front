import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs';
import { User } from './user';

export interface AuthState {
  user: User | null;
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  state: AuthState = {
    user: null
  };

  constructor(private http: HttpClient) {
    const stored = localStorage.getItem('user');
    if (stored) {
      this.state.user = JSON.parse(stored);
    }
  }

  login(name: string, password: string) {
    return this.http.get<User>('/api/user/account', {
      headers: {
        'Authorization': 'Basic ' + btoa(name + ':' + password),
      }
    }).pipe(
      tap(data => this.updateUser(data))
    );
  }

  register(user: User) {
    return this.http.post<User>('/api/user', user).pipe(
      tap(data => this.updateUser(data)) //On assigne user connectée au state
    );
  }

  logout() {
    return this.http.get<void>('/logout').pipe(
      tap(() => this.updateUser(null))
    );
  }

  update(user: User) {
    return this.http.put<User>('/api/user/' + user.id, user).pipe(
      tap(data => this.updateUser(data))
    );
  }

  changePassword(oldPassword: string, newPassword: string) {
    return this.http.patch<void>('/api/user/password', { oldPassword, newPassword });
  }

  private updateUser(data: User | null) {
    this.state.user = data;
    if (data) {
      localStorage.setItem('user', JSON.stringify(data));
    } else {
      localStorage.removeItem('user');
    }
  }
}
