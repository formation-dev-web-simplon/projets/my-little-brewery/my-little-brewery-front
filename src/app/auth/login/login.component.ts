import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NgbDropdown } from '@ng-bootstrap/ng-bootstrap';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  form = this.fb.group({
    name: ['', Validators.required],
    password: ['', Validators.required]
  });

  hasError = false;

  constructor(
    private auth: AuthService,
    private fb: FormBuilder,
    private dropdown: NgbDropdown
  ) { }

  ngOnInit(): void {
    this.dropdown.openChange.subscribe(
      () => {
        if (!this.dropdown.isOpen()) {
          this.form.reset();
          this.hasError = false;
        }
      }
    )
  }

  get name() {
    return this.form.get('name');
  }
  get password() {
    return this.form.get('password');
  }

  close() {
    this.dropdown.close();
  }

  submitForm() {
    if (this.name?.value && this.password?.value) {
      this.hasError = false;
      this.auth.login(this.name.value, this.password.value).subscribe({
        next: () => this.dropdown.close(),
        error: () => this.hasError = true
      });
    }
  }
}
