import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NgbAccordionConfig } from '@ng-bootstrap/ng-bootstrap';
import { Category } from 'src/app/item/category';
import { RecordService } from 'src/app/record/record.service';
import { Item } from '../item';
import { ItemService } from '../item.service';

@Component({
  selector: 'app-item-list[category]',
  templateUrl: './item-list.component.html',
  styleUrls: ['./item-list.component.scss']
})
export class ItemListComponent implements OnInit {

  @Input()
  category!: Category;

  @Output()
  newItemCat: EventEmitter<void> = new EventEmitter();

  constructor(
    private itemService: ItemService,
    private recordService: RecordService,
    public accordionConfig: NgbAccordionConfig
  ) {
    accordionConfig.closeOthers = true;
  }

  ngOnInit(): void {
  }

  detail(item: Item) {
    if (item.id) {
      this.recordService.showByItemId(item.id)
        .subscribe(data => item.records = data)
    }
  }

  newItem() {
    this.newItemCat.emit();
  }

  removeItem(itemToRemove: Item) {
    if (itemToRemove.id) {
      this.itemService.delete(itemToRemove.id).subscribe({
        next: () => {
          const idx = this.category.items.findIndex(item => item.id == itemToRemove.id)
          if (idx >= 0) {
            this.category.items.splice(idx, 1);
          }
        }
      });
    }
  }
}
