import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { catDesc } from '../category';
import { Item } from '../item';
import { ItemService } from '../item.service';

@Component({
  selector: 'app-item-edit',
  templateUrl: './item-edit.component.html',
  styleUrls: ['./item-edit.component.scss']
})
export class ItemEditComponent implements OnInit {

  @Input()
  item?: Item;

  form?: FormGroup;

  constructor(
    private fb: FormBuilder,
    private itemService: ItemService,
    public activeModal: NgbActiveModal
  ) { }

  ngOnInit(): void {
    if (this.item != undefined) {
      let type = catDesc.various.type
      for (const key in catDesc) {
        if (this.item.type == (<any>catDesc)[key].type) {
          type = this.item.type
        }
      }
      this.form = this.fb.group({
        name: [this.item.name, [Validators.required, Validators.minLength(3)]],
        brand: [this.item.brand, [Validators.required, Validators.minLength(3)]],
        type: [{ value: type, disabled: true }],
      });
    }
  }

  sendForm() {
    let itemForm: Item = this.form?.value;
    if (this.item) {
      this.item.name = itemForm.name;
      this.item.brand = itemForm.brand;
      this.itemService.update(this.item).subscribe({
        next: data => {
          this.item = data;
          this.activeModal.close();
        }
      })
    }
  }
}
