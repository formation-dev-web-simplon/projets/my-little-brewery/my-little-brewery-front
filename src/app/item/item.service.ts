import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { catDesc, Category } from './category';
import { Item } from './item';
@Injectable({
  providedIn: 'root'
})
export class ItemService {

  private url: string;

  constructor(
    private http: HttpClient
  ) {
    this.url = '/api/item';
  }

  public showAll(): Observable<Category[]> {
    return this.http.get<Item[]>(this.url).pipe(
      map(data => this.orderByCategories(data))
    );
  }

  public showOne(id: number): Observable<Item> {
    return this.http.get<Item>(this.url + '/' + id)
  }

  public add(item: Item): Observable<Item> {
    return this.http.post<Item>(this.url, item);
  }

  public update(item: Item): Observable<Item> {
    return this.http.put<Item>(this.url + '/' + item.id, item);
  }

  public delete(id: number): Observable<void> {
    return this.http.delete<void>(this.url + '/' + id);
  }

  public orderByCategories(items: Item[]) {
    const categories: Category[] = [];
    for (const type in catDesc) {
      const category = {
        label: (<any>catDesc)[type].label,
        type: (<any>catDesc)[type].type,
        unit: (<any>catDesc)[type].unit,
        items: []
      }
      categories.push(category);
    }
    for (let item of items) {
      this.addItemToCategory(item, categories);
    }
    return categories;
  }

  public addItemToCategory(item: Item, categories: Category[]) {
    let category = categories.find(cat => cat.type == item.type);
    if (!category) {
      category = categories.find(cat => cat.type == catDesc.various.type);
    }
    category!.items.push(item);
  }
}
