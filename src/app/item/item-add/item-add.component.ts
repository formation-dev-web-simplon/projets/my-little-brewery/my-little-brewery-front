import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Item } from '../item';
import { ItemService } from '../item.service';

@Component({
  selector: 'app-item-add',
  templateUrl: './item-add.component.html',
  styleUrls: ['./item-add.component.scss']
})
export class ItemAddComponent implements OnInit {

  @Input()
  type?: string;

  @Output()
  newItem: EventEmitter<Item> = new EventEmitter();

  form?: FormGroup;

  constructor(
    private fb: FormBuilder,
    private itemService: ItemService,
    public activeModal: NgbActiveModal
  ) { }

  ngOnInit(): void {
    if (this.type != undefined) {
      this.form = this.fb.group({
        name: ['', [Validators.required, Validators.minLength(3)]],
        brand: ['', [Validators.required, Validators.minLength(3)]],
        type: [this.type],
      });
    }
  }

  sendForm() {
    let itemForm: Item = this.form?.value;
    itemForm.quantity = 0;
    this.itemService.add(itemForm).subscribe({
      next: data => {
        this.newItem.emit(data);
        this.activeModal.close();
      }
    })
  }
}
