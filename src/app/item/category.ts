import { CurrencyPipe, DatePipe } from "@angular/common";
import { debounceTime, distinctUntilChanged, map, Observable, OperatorFunction } from "rxjs";
import { Item } from "./item";

export interface Category {
  label: string;
  type: string;
  unit: string;
  items: Item[];
}

export interface Display {
  label: string;
  property: string;
  display?: string;
  pipe?: (value: string) => string | null;
  addUnit?: string;
  type?: string;
  validators?: undefined;
  required?: boolean;
  min?: number;
  max?: number;
  step?: number;
  date?: string;
}

const pipeDate = (value: string) => new DatePipe('fr').transform(value, 'dd MMM yyyy');
const pipePercentage = (value: string) => value + ' %';
const pipeCurrency = (value: string) => new CurrencyPipe('fr').transform(value, 'EUR', 'symbol', '1.2-2');

const pipeDatetoString = (value: Date) => new DatePipe('fr').transform(value, 'yyyy-MM-dd');

const origins = ['Afrique du Sud', 'Allemagne', 'Angleterre', 'Argentine', 'Australie', 'Autriche', 'Belgique', 'Canada', 'Chine', 'Etats Unis',
  'France', 'Irlande', 'Japon', 'Nouvelle-Zélande', 'Pologne', 'République Tchèque', 'Royaume-Uni', 'Serbie', 'Slovénie', 'Ukraine'];
const searchOrigin: OperatorFunction<string, readonly string[]> = (text$: Observable<string>) =>
  text$.pipe(
    debounceTime(200),
    distinctUntilChanged(),
    map(term => term.length < 1 ? []
      : origins.filter(value => value.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
  );
  
const shapes = ['Cône', 'Cryo', 'Pellet'];
const searchShape: OperatorFunction<string, readonly string[]> = (text$: Observable<string>) =>
  text$.pipe(
    debounceTime(200),
    distinctUntilChanged(),
    map(term => term.length < 1 ? []
      : shapes.filter(value => value.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
  );

const common = [
  { label: 'Date stockage', property: 'date', display: 'col-lg-3 d-none d-lg-block order-first', pipe: pipeDate, type: 'date', required: true, date: 'pastOrPresent', max: pipeDatetoString(new Date()) },
  { label: 'Coût', property: 'cost', pipe: pipeCurrency, addUnit: 'costUnit', type: 'number', min: 0, step: 0.01 },
  { label: 'Quantité', property: 'quantity', display: 'col-4 col-lg-3 order-5', addUnit: 'unit', type: 'number', required: true, min: 0, step: 0.1 },
] as Display[];

export const catDesc = {
  fermentable: {
    label: 'Céréales et sucres',
    type: 'fermentable',
    unit: 'kg',
    costUnit: '/ kg',
    toDisplay: [
      ...common,
      { label: 'Conditionnement', property: 'conditioning', addUnit: 'unit', type: 'number', required: true, min: 0, step: 0.1 },
      { label: 'DLUO', property: 'useByDate', pipe: pipeDate, display: 'col-4 col-lg-3', type: 'date', required: true },
      { label: 'EBC', property: 'ebc', display: 'col-4 col-lg-3', type: 'number', required: true, min: 0, step: 0.1 },
    ] as Display[]
  },
  hop: {
    label: 'Houblons',
    type: 'hop',
    unit: 'g',
    costUnit: '/ 100g',
    toDisplay: [
      ...common,
      { label: 'Conditionnement', property: 'conditioning', addUnit: 'unit', type: 'number', required: true, min: 0, step: 0.1 },
      { label: 'DLUO', property: 'useByDate', pipe: pipeDate, type: 'date' },
      { label: 'Année', property: 'harvest', display: 'col-4 col-lg-3', type: 'number', required: true, min: new Date().getFullYear() - 10, max: new Date().getFullYear(), noDecimal: true },
      { label: 'Acide α', property: 'alpha', pipe: pipePercentage, display: 'col-4 col-lg-3', type: 'number', required: true, min: 0, step: 0.1 },
      { label: 'Forme', property: 'shape', search: searchShape },
      { label: 'Pays', property: 'origin', required: true, search: searchOrigin },
    ] as Display[]
  },
  yeast: {
    label: 'Levures',
    type: 'yeast',
    unit: 'sachet(s)',
    costUnit: '/ sachet',
    toDisplay: [
      ...common,
      { label: 'Conditionnement', property: 'conditioning', addUnit: 'unit', type: 'number', required: true, min: 0, step: 0.1 },
      { label: 'DLUO', property: 'useByDate', pipe: pipeDate, display: 'col-4 col-lg-3', type: 'date', required: true },
      { label: 'Atténuation', property: 'attenuation', pipe: pipePercentage, display: 'col-4 col-lg-3', type: 'number', required: true, min: 0, step: 0.1 },
    ] as Display[]
  },
  various: {
    label: 'Divers',
    type: '',
    toDisplay: [
      ...common,
      { label: 'Conditionnement', property: 'conditioning', display: 'col-4 col-lg-3 order-2', type: 'number', required: true, min: 0, step: 0.1 },
      { label: 'DLUO', property: 'useByDate', pipe: pipeDate, display: 'col-4 col-lg-3 order-1', type: 'date' },
    ] as Display[]
  }
};