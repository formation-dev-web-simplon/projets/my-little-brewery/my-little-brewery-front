import { Record } from "../record/record";

export interface Item {
  id?: number;
  name: string;
  brand: string;
  type: string;
  records: Record[];
  quantity: number;
}