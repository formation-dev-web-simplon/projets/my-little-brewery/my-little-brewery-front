import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AuthService } from 'src/app/auth/auth.service';
import { ConfirmationComponent } from 'src/app/modal/confirmation/confirmation.component';
import { RecordAddComponent } from 'src/app/record/record-add/record-add.component';
import { Item } from '../item';
import { ItemEditComponent } from '../item-edit/item-edit.component';

@Component({
  selector: 'app-item[item]',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss']
})
export class ItemComponent implements OnInit {

  @Input()
  item?: Item;

  @Output()
  removedItem: EventEmitter<void> = new EventEmitter();

  authState = this.auth.state;

  constructor(
    private auth: AuthService,
    private modalService: NgbModal
  ) { }

  ngOnInit(): void {

  }

  editItem() {
    const editModalRef = this.modalService.open(ItemEditComponent, { centered: true, backdrop: 'static' });
    editModalRef.componentInstance.item = this.item;
  }

  deleteItem() {
    const confModalRef = this.modalService.open(ConfirmationComponent, { centered: true, backdrop: 'static' });
    confModalRef.componentInstance.title = 'Suppression produit';
    confModalRef.componentInstance.choice.subscribe((choice: boolean) => {
      if (choice) {
        this.removedItem.emit()
      }
    })
  }

  newRecord() {
    const newModalRef = this.modalService.open(RecordAddComponent, { centered: true, backdrop: 'static' });
    newModalRef.componentInstance.item = this.item;
  }
}
