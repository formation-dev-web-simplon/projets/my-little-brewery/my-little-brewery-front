import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { catDesc, Category } from 'src/app/item/category';
import { Item } from '../item';

@Component({
  selector: 'app-item-form[form]',
  templateUrl: './item-form.component.html',
  styleUrls: ['./item-form.component.scss']
})
export class ItemFormComponent implements OnInit {
  @Input()
  form?: FormGroup;

  categories: Category[] = [];

  constructor(
  ) { }

  ngOnInit(): void {
    for (const cat in catDesc) {
      this.categories.push((<any>catDesc)[cat])
    }
  }

  get name() {
    return this.form?.get('name');
  }
  get brand() {
    return this.form?.get('brand');
  }
  get type() {
    return this.form?.get('type');
  }
}
