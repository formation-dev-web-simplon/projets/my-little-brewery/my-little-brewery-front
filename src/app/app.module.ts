import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import '@angular/common/locales/global/fr';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { ItemListComponent } from './item/item-list/item-list.component';
import { HeaderComponent } from './header/header.component';
import { CredentialInterceptor } from './credential.interceptor';
import { ItemComponent } from './item/item/item.component';
import { RecordListComponent } from './record/record-list/record-list.component';
import { ItemFormComponent } from './item/item-form/item-form.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ItemAddComponent } from './item/item-add/item-add.component';
import { ItemEditComponent } from './item/item-edit/item-edit.component';
import { RecordFormComponent } from './record/record-form/record-form.component';
import { RecordComponent } from './record/record/record.component';
import { RecordEditComponent } from './record/record-edit/record-edit.component';
import { RecordAddComponent } from './record/record-add/record-add.component';
import { FooterComponent } from './footer/footer.component';
import { LoginComponent } from './auth/login/login.component';
import { ChangePwdComponent } from './auth/change-pwd/change-pwd.component';
import { ConfirmationComponent } from './modal/confirmation/confirmation.component';
import { AlertComponent } from './modal/alert/alert.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ItemListComponent,
    HeaderComponent,
    ItemComponent,
    RecordListComponent,
    ItemFormComponent,
    ItemAddComponent,
    ItemEditComponent,
    ConfirmationComponent,
    RecordFormComponent,
    RecordComponent,
    RecordEditComponent,
    RecordAddComponent,
    FooterComponent,
    LoginComponent,
    ChangePwdComponent,
    AlertComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    NgbModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: CredentialInterceptor, multi: true }],
  bootstrap: [AppComponent]
})
export class AppModule { }
