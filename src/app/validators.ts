import { AbstractControl, ValidationErrors, ValidatorFn } from "@angular/forms";

export function passwordMatchValidator(control: AbstractControl): ValidationErrors | null {
  const password: string = control.get('newPassword')?.value;
  const confirmPassword: string = control.get('repeatPassword')?.value;
  return password !== confirmPassword ? { 'NoPassswordMatch': true } : null;
}

export function noDecimalValidator(): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    const value = control.value;
    if (value) {
      const noDecimalCase = /^[0-9]+$/.test(value);
      return !noDecimalCase ? { noDecimal: true } : null;
    }
    return null;
  }
}

export function pastOrPresent(): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    const value = control.value;
    if (value) {
      const today = new Date().getTime();
      return new Date(value).getTime() > today ? { 'pastOrPresent': true } : null;
    }
    return null;
  }
}