import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-confirmation',
  templateUrl: './confirmation.component.html',
  styleUrls: ['./confirmation.component.scss']
})
export class ConfirmationComponent implements OnInit {

  @Input()
  title = 'Confirmation';

  @Output()
  choice: EventEmitter<boolean> = new EventEmitter()

  constructor(public activeModal:NgbActiveModal) { }

  ngOnInit(): void {
  }

  sendChoice(choice:boolean){
    this.choice.emit(choice);
    this.activeModal.close();
  }

}
